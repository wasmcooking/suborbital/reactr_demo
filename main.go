package main

import (
	//"bytes"
	"fmt"
	"unsafe"

	"github.com/suborbital/reactr/rt"
	"github.com/suborbital/reactr/rwasm"
)

func encodeUnsafe(fs []float32) []byte {
	return unsafe.Slice((*byte)(unsafe.Pointer(&fs[0])), len(fs)*4)
}

func decodeUnsafe(bs []byte) []float32 {
	return unsafe.Slice((*float32)(unsafe.Pointer(&bs[0])), len(bs)/4)
}
// https://www.reddit.com/r/golang/comments/qctyhx/what_is_the_best_way_to_covert_slice_of_floats_to/
func main() {
	r := rt.New()

	
	doHelloWasm := r.Register("hello", rwasm.NewRunner("./hello/hello.wasm"))

	data := []float32{12.5, 34.8}
	result, err := doHelloWasm(encodeUnsafe(data)).Then()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(result.([]byte)))

}
