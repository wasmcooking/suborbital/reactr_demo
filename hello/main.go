package main

import (
	"fmt"
	"unsafe"

	"github.com/suborbital/reactr/api/tinygo/runnable"
)

func decodeUnsafe(bs []byte) []float32 {
	return unsafe.Slice((*float32)(unsafe.Pointer(&bs[0])), len(bs)/4)
}


type Hello struct{}

func (h Hello) Run(input []byte) ([]byte, error) {
	data := decodeUnsafe(input)
	fmt.Println(data)

	return []byte("Hello"), nil
}

// initialize runnable, do not edit //
func main() {
	runnable.Use(Hello{})
}
